var mymap = L.map('mapid').setView([45.590325, 5.920590], 13);
/*Pointeur Freelance*/
var marker = L.marker([45.571982551298305, 5.912269403795591]).addTo(mymap);
// marker.bindPopup("Freelance").openPopup();
/*End Freelance*/

/*var circle = L.circle([45.590325, 5.920590], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 2500
}).addTo(mymap);*/


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,})
    .addTo(mymap);





