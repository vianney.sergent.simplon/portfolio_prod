<?php

namespace App\Uploader;

use Symfony\Component\HttpFoundation\File\UploadedFile;


interface UploaderFileInterface
{
    /**
     * Undocumented function
     *
     * @param UploadedFile $file
     * @return string|null
     */
    public function upload(UploadedFile $file): ?string;
    /**
     * Undocumented function
     *
     * @param string $path
     * @return void
     */
    public function removeFile(string $path);
}
