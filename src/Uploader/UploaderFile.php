<?php

namespace App\Uploader;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;

class UploaderFile implements UploaderFileInterface
{
    /**
     * Undocumented variable
     *
     * @var SluggerInterface
     */
    protected $slugger;
    /**
     * @var ParameterBagInterface
     */
    protected $parameterBag;

    public function __construct(SluggerInterface $slugger, ParameterBagInterface $parameterBag)
    {
        $this->slugger = $slugger;
        $this->parameterBag = $parameterBag;
    }
    /**
     * Undocumented function
     *
     * @param UploadedFile $file
     * @return File|null
     */
    public function upload(UploadedFile $file): ?string
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        // this is needed to safely include the file name as part of the URL
        $safeFilename = $this->slugger->slug($originalFilename);
        $newFilename = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

        // Move the file to the directory where brochures are stored
        try {
            return $file->move(
                $this->parameterBag->get('File_directory'),
                $newFilename
            );
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }
    }
    /**
     * Undocumented function
     *
     * @param string $path
     * @return void
     */
    public function removeFile(string $path)
    {
        $filesystem = new Filesystem();
        $filesystem->remove($path);
    }
}
