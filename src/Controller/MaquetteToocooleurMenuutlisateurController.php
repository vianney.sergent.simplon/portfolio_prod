<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MaquetteToocooleurMenuutlisateurController extends AbstractController
{
    /**
     * @Route("/maquette/toocooleur/menu-utlisateur", name="maquette_toocooleur_menu-utlisateur")
     */
    public function index(): Response
    {
        return $this->render('maquette_toocooleur_menuutlisateur/maquette_toocooleur_menu-utlisateur.html.twig', [
            'tab_name' => 'Maquette-Toocooleur-Menu Utilisateur',
            'page_title' => 'Maquettes Toocooleur-Menu Utilisateur'
        ]);
    }
}
