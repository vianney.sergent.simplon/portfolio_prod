<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MentionsController extends AbstractController
{
    /**
     * @Route("/mentions", name="mentions")
     */
    public function index(): Response
    {
        return $this->render('mentions/mentions.html.twig', [
            'tab_name' => 'Développeur web -Vianney SERGENT-Mentions Légale',
            'page_title' => 'Mentions Légales'
        ]);
    }
}
