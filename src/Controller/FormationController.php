<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormationController extends AbstractController
{
    /**
     * @Route("/formations", name="formations")
     */
    public function index(): Response
    {
        return $this->render('formations/formations.html.twig', [
            'tab_name' => 'Développeur web freelance-Vianney SERGENT-Mes Formations',
            'page_title' => "Mes Formations"
        ]);
    }
}