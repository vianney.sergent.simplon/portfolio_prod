<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MaquetteToocooleurPageGeneralisteController extends AbstractController
{
    /**
     * @Route("/maquette/toocooleur/page-generaliste", name="maquette-toocooleur-page-generaliste")
     */
    public function index(): Response
    {
        return $this->render('maquette_toocooleur_page_generaliste/maquette-toocooleur-page-generaliste.html.twig', [
            'tab_name' => 'Maquette-Toocooleur-Page Genéraliste',
            'page_title' => 'Maquettes Toocooleur-Page Genéraliste'
        ]);
    }
}
