<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AProposController extends AbstractController
{
    /**
     * @Route("/a-propos", name="a-propos")
     */
    public function index()
    {
        return $this->render('a-propos/a-propos.html.twig', [
            'tab_name' => 'Développeur web -Vianney SERGENT-A Propos',
            'page_title' => 'A propos'
        ]);
    }
}
