<?php

namespace App\Controller;

use App\Form\QuestionnaireType;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class QuestionnaireController extends AbstractController
{
    /**
     * @Route("/questionnaire", name="questionnaire")
     */
    public function index(Request $request, MailerInterface $mailer)
    {
        $form = $this->createForm(QuestionnaireType::class);

        $questionnaire = $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Ici on envoie l'email

            $email = (new TemplatedEmail())
                ->from($questionnaire->get('email')->getData())
                ->to('vianney.dev73@gmail.com')
                ->htmlTemplate('emails/questionnaire.html.twig')
                ->context([
                    'Presentation' => $questionnaire->get('Presentation')->getData(),
                    'Objectif' => $questionnaire->get('Objectif')->getData(),
                    'Cible' => $questionnaire->get('Cible')->getData(),
                    'Fonctionnalite' => $questionnaire->get('Fonctionnalite')->getData(),
                    'Chartre' => $questionnaire->get('Chartre')->getData(),
                    'Site' => $questionnaire->get('site')->getData(),
                    'mail' => $questionnaire->get('email')->getData(),
                ]);

            $mailer->send($email);

            // Permet un message flash de renvoi

            $this->addFlash('message', 'Votre message a été transmis, nous vous répondrons dans les meilleurs délais.');

            // Confirmation de l'envoie du message

            // $this->addFlash('message', 'Votre e-mail a bien été envoyé');

            // on redirige vers la page contact

            return $this->redirectToRoute('depose-dossier');
        }
        return $this->render('questionnaire/questionnaire.html.twig', [
            'QuestionnaireForm' => $form->createView(),
            'tab_name' => 'Développeur web -Vianney SERGENT-Questionnaire Entretien',
            'page_title' => 'Questionnaire Entretien'
        ]);
    }
}
