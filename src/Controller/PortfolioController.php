<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
     * @Route("/projets-realisations")
*/

class PortfolioController extends AbstractController
{
    /**
     * @Route("/", name="projets-realisations")
     */
    public function index()
    {
        return $this->render('portfolio/portfolio.html.twig', [
            'tab_name' => 'Développeur web -Vianney SERGENT-Portfolio',
            'page_title' => 'Mes Réalisations'
        ]);
    }
    /**
     * @Route("/retroloc", name="retroloc")
     */
    public function retroloc()
    {
        return $this->render('portfolio/retroloc.html.twig', [
            'tab_name' => 'Développeur web -Vianney SERGENT-Portfolio',
            'page_title' => 'Retroloc'
        ]);
    }
    /**
     * @Route("/little_big", name="little_big")
     */
    public function littleBig()
    {
        return $this->render('portfolio/littlebig.html.twig', [
            'tab_name' => 'Développeur web -Vianney SERGENT-Portfolio',
            'page_title' => 'LittleBig'
        ]);
    }
    /**
     * @Route("/too_cooleur", name="too_cooleur")
     */
    public function too_cooleur()
    {
        return $this->render('portfolio/too_cooleur.html.twig', [
            'tab_name' => 'Développeur web -Vianney SERGENT-Portfolio',
            'page_title' => 'Too_Cooleur'
        ]);
    }
     /**
     * @Route("/ms-remplacement", name="ms-remplacement")
     */
    public function msremplacement()
    {
        return $this->render('portfolio/ms-remplacement.html.twig', [
            'tab_name' => 'Développeur web -Vianney SERGENT-Portfolio',
            'page_title' => 'MS-Remplacement'
        ]);
    }
}