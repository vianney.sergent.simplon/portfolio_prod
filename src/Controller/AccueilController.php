<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AccueilController extends AbstractController
{
    /**
     * @Route("/", name="Accueil")
     */
    public function index()
    {
        return $this->render('Accueil/index.html.twig', [
            'Vianney_Sergent' => ' Vianney Sergent ',
            'tab_name' => 'Vianney SERGENT-Portfolio-Développeur web',
            'Developpeur_web' => 'Portfolio-Développeur Web',
            // 'Vitrine_E_Commerce' => 'Vitrine,E-Commerce,',
            // 'Vitrine_E_Commerce_Evenementiel_mobile' => 'Vitrine,E-Commerce,Evenementiel',
            // 'Evenementiel' => 'Evenementiel'
        ]);
    }
}