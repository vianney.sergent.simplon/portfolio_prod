<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MaquetteToocooleurPanierController extends AbstractController
{
    /**
     * @Route("/maquette/toocooleur/panier", name="maquette-toocooleur-panier")
     */
    public function index(): Response
    {
        return $this->render('maquette_toocooleur_panier/maquette-toocooleur-panier.html.twig', [
            'tab_name' => 'Maquette-Toocooleur-Panier',
            'page_title' => 'Maquettes Toocooleur-Panier'
        ]);
    }
}
