<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function index(Request $request)
    {
        // on récupère le nom d'hote depuis l'URL
        $hostname = $request->getSchemeAndHttpHost();

        // On initialise un tableau pour lister les URLs
        $urls = []; // ou array()
        // on ajoutes les URLs "statiques"
        $urls[] = ['loc' => $this->generateUrl('Accueil')];
        $urls[] = ['loc' => $this->generateUrl('a-propos')];
        $urls[] = ['loc' => $this->generateUrl('competences')];
        $urls[] = ['loc' => $this->generateUrl('contact')];
        $urls[] = ['loc' => $this->generateUrl('langage')];
        $urls[] = ['loc' => $this->generateUrl('portfolio')];
        $urls[] = ['loc' => $this->generateUrl('depose-dossier')];
        $urls[] = ['loc' => $this->generateUrl('service')];
        $urls[] = ['loc' => $this->generateUrl('logiciel')];
        $urls[] = ['loc' => $this->generateUrl('mentions')];
        $urls[] = ['loc' => $this->generateUrl('prestation')];
        $urls[] = ['loc' => $this->generateUrl('tarif')];


        // fabriquer de la réponse
        $response = new Response(
            $this->renderView('sitemap/index.html.twig', [
                'urls' => $urls,
                'hostname' => $hostname
            ]),
            200
        );

        // ajout des entêtes HTTP
        $response->headers->set('Content-type', 'text/xml');

        // on envoie la réponse
        return $response;
    }
}
