<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ServiceController extends AbstractController
{
    /**
     * @Route("/service", name="service")
     */
    public function index2()
    {
        return $this->render('service/service.html.twig', [
            'tab_name' => 'Développeur web-Vianney SERGENT-Services',
            'page_title' => 'atouts'
        ]);
    }
}
