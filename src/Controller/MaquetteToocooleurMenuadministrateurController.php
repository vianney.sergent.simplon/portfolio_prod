<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MaquetteToocooleurMenuadministrateurController extends AbstractController
{
    /**
     * @Route("/maquette/toocooleur/menu-administrateur", name="maquette-toocooleur-menu-administrateur")
     */
    public function index(): Response
    {
        return $this->render('maquette_toocooleur_menuadministrateur/maquette-toocooleur-menu-administrateur.html.twig', [
            'tab_name' => 'Maquette-Toocooleur-Menu administrateur',
            'page_title' => 'Maquettes Toocooleur-Menu administrateur'
        ]);
    }
}
