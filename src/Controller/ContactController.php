<?php

namespace App\Controller;

use ReCaptcha\ReCaptcha;
use App\Form\ContactType;
use App\Uploader\UploaderFileInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */

    public function index(Request $request, MailerInterface $mailer, UploaderFileInterface $uploaderFile)
    {
        /*$this->newFile($request, $slugger);*/

        // $form = $this->createForm(ContactType::class);
        // $contact = $form->handleRequest($request);

        // if ($form->isSubmitted() && $form->isValid()) {

        //     // $contactFormData = $form->getData();


        //     // Ici on envoie l'email

        //     $file = $form->get('piecejointe')->getData();

        //     $upload = false;
        //     if ($file) {
        //         $upload = $uploaderFile->upload($file);
        //     }
        //     // dd($upload);

        //     $email = (new TemplatedEmail())
        //         ->from($contact->get('email')->getData())
        //         ->to('vianney.dev73@gmail.com')
        //         ->subject($contact->get('sujet')->getData());
        //         if($upload){
        //             $email->attachFromPath($upload);
        //         }
        //         // ->attachFromPath($upload)
        //        $email->htmlTemplate('emails/contact.html.twig')
        //         ->context([
        //             'nom' => $contact->get('nom')->getData(),
        //             'mail' => $contact->get('email')->getData(),
        //             'message' => $contact->get('message')->getData(),
        //             'phone' => $contact->get('telephone')->getData()

        //         ]);


        //     $mailer->send($email);
        //     if($upload){
        //         $uploaderFile->removeFile($upload);
        //     }

        //     // Permet un message flash de renvoi

        //     $this->addFlash('message', 'Votre message a été transmis, nous vous répondrons dans les meilleurs délais');
        //     $this->addFlash('uploadMessage', 'Votre fichier a bien été telechargé, nous en prendrons connaissance bientot');

        //     // $this->addFlash('message', 'Votre e-mail a bien été envoyé');

        //     // on redirige vers la page contact

        //     return $this->redirectToRoute('contact');
        // }
        return $this->render('contact/contact_page.html.twig', [
            // 'ContactForm' => $form->createView(),
            'tab_name' => 'Développeur web -Vianney SERGENT-Contact',
            'page_title' => "Contact",

        ]);
    }



}