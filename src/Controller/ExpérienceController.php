<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExpérienceController extends AbstractController
{
    /**
     * @Route("experience", name="experience")
     */
    public function index(): Response
    {
        return $this->render('expérience/Expériences.html.twig', [
            'tab_name' => 'Développeur web freelance-Vianney SERGENT-Mes Expériences',
            'page_title' => "Mes Expériences"
        ]);
    }
}