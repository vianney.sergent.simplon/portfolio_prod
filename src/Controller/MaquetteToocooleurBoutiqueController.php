<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MaquetteToocooleurBoutiqueController extends AbstractController
{
    /**
     * @Route("/maquette-toocooleur-boutique", name="maquette-toocooleur-boutique")
     */
    public function index(): Response
    {
        return $this->render('maquette_toocooleur_boutique/maquette-toocooleur-boutique.html.twig', [
            'tab_name' => 'Maquette-Toocooleur-Boutique',
            'page_title' => 'Maquettes Toocooleur-Boutique'
        ]);
    }
}
