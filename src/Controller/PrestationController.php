<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PrestationController extends AbstractController
{
    /**
     * @Route("/prestation", name="prestation")
     */
    public function index(): Response
    {
        return $this->render('prestation/prestation.html.twig', [
            'tab_name' => 'Développeur web -Vianney SERGENT-Prestations',
            'page_title' => 'Prestation'
        ]);
    }
}
