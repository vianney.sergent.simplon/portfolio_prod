<?php

namespace App\Controller;

use App\Form\ProjetType;
use App\Uploader\UploaderFileInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProjetController extends AbstractController
{
    /**
     * @Route("/depose-dossier", name="depose-dossier")
     */
    public function index(Request $request, MailerInterface $mailer, UploaderFileInterface $uploaderFile)
    {
        $form = $this->createForm(ProjetType::class);

        $contact = $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $form->get('piecejointe')->getData();


            if ($file) {
                $upload = $uploaderFile->upload($file);
            }

            // Ici on envoie l'email

            $email = (new TemplatedEmail())
                ->from($contact->get('email')->getData())
                ->to('vianney.dev73@gmail.com')
                ->attachFromPath($upload)
                ->htmlTemplate('emails/project.html.twig')
                ->context([
                    'nom' => $contact->get('nom')->getData(),
                    'type' => $contact->get('type')->getData(),
                    'mail' => $contact->get('email')->getData(),
                    'phone' => $contact->get('telephone')->getData(),
                    'adresse_site' => $contact->get('adresse_site')->getData(),
                    'projet' => $contact->get('projet')->getData(),
                    'Date_limite' => $contact->get('Date_limite')->getData(),
                    'message' => $contact->get('message')->getData()
                ]);

            $mailer->send($email);
            $uploaderFile->removeFile($upload);

            // Permet un message flash de renvoi

            $this->addFlash('message', 'Votre message a été transmis, nous vous répondrons dans les meilleurs délais.');

            // Confirmation de l'envoie du message

            // $this->addFlash('message', 'Votre e-mail a bien été envoyé');

            // on redirige vers la page questionnaire

            return $this->redirectToRoute('questionnaire');
        }
        return $this->render('projet/projet.html.twig', [
            'projectForm' => $form->createView(),
            'tab_name' => 'Développeur web -Vianney SERGENT-Depose dossiers',
            'page_title' => 'Dépose dossier'

        ]);
    }
}
