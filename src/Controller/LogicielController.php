<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LogicielController extends AbstractController
{
    /**
     * @Route("/logiciel", name="logiciel")
     */
    public function index(): Response
    {
        return $this->render('logiciel/logiciel.html.twig', [
            'tab_name' => 'Développeur web -Vianney SERGENT-Mes logiciels',
            'page_title' => 'Mes logiciels'
        ]);
    }
}
