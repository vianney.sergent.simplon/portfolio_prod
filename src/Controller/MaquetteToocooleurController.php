<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MaquetteToocooleurController extends AbstractController
{
    /**
     * @Route("/maquette-toocooleur", name="maquette-toocooleur")
     */
    public function index(): Response
    {
        return $this->render('maquette_toocooleur/maquette-toocooleur.html.twig', [
            'tab_name' => 'Développeur web -Vianney SERGENT-Maquette-Toocooleur',
            'page_title' => 'Maquettes Toocooleur'
        ]);
    }
}
