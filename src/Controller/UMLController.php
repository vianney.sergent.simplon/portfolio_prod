<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UMLController extends AbstractController
{
    /**
     * @Route("/uml", name="UML")
     */
    public function index(): Response
    {
        return $this->render('uml/Diagramme-UML.html.twig', [
            'tab_name' => 'Développeur web freelance-Vianney SERGENT-Diagramme UML',
            'page_title' => 'Diagramme UML'
        ]);
    }
    /**
     * @Route("/uml/toocooleur", name="UML-toocooleur")
     */
    public function showToocoleur(): Response
    {
        return $this->render('uml/toocooleur-diagramme-uml.twig', [
            'tab_name' => 'Développeur web freelance-Vianney SERGENT-Diagramme UML',
            'page_title' => 'Diagramme UML-Toocooleur'
        ] );
    }
}
