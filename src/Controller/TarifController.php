<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TarifController extends AbstractController
{
    /**
     * @Route("/tarif", name="tarif")
     */
    public function index(): Response
    {
        return $this->render('tarif/Tarification.html.twig', [
            'tab_name' => 'Développeur web-Vianney SERGENT-Tarif',
            'page_title' => 'Tarification'
        ]);
    }
}
