<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MaquetteController extends AbstractController
{
    /**
     * @Route("/maquette", name="maquette")
     */
    public function index(): Response
    {
        return $this->render('maquette/maquette.html.twig', [
            'tab_name' => 'Développeur web-Vianney SERGENT-Maquette',
            'page_title' => 'Mes maquettes'
        ]);
    }
}
