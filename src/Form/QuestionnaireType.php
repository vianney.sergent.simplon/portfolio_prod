<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class QuestionnaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Presentation', TextareaType::class,  [
                'attr' => ['cols' => 20, 'rows' => 5],
                'label' => 'Présentez votre association (histoire, service, secteur …) ? ',
                'required' => true,
            ])
            ->add('Objectif', TextareaType::class,  [
                'attr' => ['cols' => 20, 'rows' => 5],
                'label' => 'Quel est l’objectif du site internet (Décrivez à quoi sert le site et quel est son objectif. Par exemple, votre site doit permettre de : soit présenter les produits ou les services de l’association, soit faire connaitre et/ou développer l’association, soit vendre vos produits ou services. Vous pouvez également préciser comment le site s’intègre, ou doit s’intégrer, dans la stratégie de communication ou commerciale globale)',
                'required' => true,
            ])
            ->add('Cible', TextareaType::class,  [
                'attr' => ['cols' => 20, 'rows' => 5],
                'label' => 'Quels sont vos cibles (usagers/adhérents/grand public) ?',
                'required' => true,
            ])
            ->add('Fonctionnalite', TextareaType::class,  [
                'attr' => ['cols' => 20, 'rows' => 5],
                'label' => 'Quel est le type de fonctionnalité que vous souhaitez sur votre site internet (Il s’agit de lister l’ensemble des fonctionnalités que vous attendez de votre site internet. Ne tombez pas dans le piège qui consiste à tout demander, chaque fonctionnalité doit présenter un réel intérêt. N’hésitez pas à mettre des exemples pour faciliter la compréhension. Voici quelques idées : Multilangue, Formulaire de contact, Calendrier de formations, Téléchargement de documents sur le site, News... Cela permet de connaitre les différents langages nécessaires à la réalisation de votre site. (Php, Javascript, HTML, SQL,...) )?',
                'required' => true,
            ])
            ->add('Chartre', TextareaType::class,  [
                'attr' => ['cols' => 20, 'rows' => 5],
                'label' => 'Possédez-vous une charte graphique (couleur, police) dans votre association ?',
                'required' => true,
            ])
            ->add('site', ChoiceType::class, array(
                'choices' => array(
                    'Site Vitrine' => 'Site Vitrine',
                    'Site-e-commerce' => 'Association',
                ),
                'expanded' => true,
                'label_attr' => [
                    'class' => 'radio-inline'
                ],
                'multiple' => false,
                'label' => 'Voulez-vous un site vitrine ou un site e-commerce ( vitrine : un site Web qui présente en ligne les produits ou les services d\'une organisation dans le but d\'attirer simplement l\'attention et d\'éveiller l\'intérêt des internautes de passage. Site e-commmerce : site de vente en ligne)',
            ))
            ->add('email', EmailType::class, [
                'label' => 'Mail où vous pouvez être joint ? ',
                'required' => true
            ])
            ->add('Envoyer', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
