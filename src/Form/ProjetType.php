<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Beelab\Recaptcha2Bundle\Form\Type\RecaptchaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Beelab\Recaptcha2Bundle\Validator\Constraints\Recaptcha2;

class ProjetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'label' => 'Nom et Prénom *',
                'label_attr' => [
                    'class' => 'form-label'
                ],
            ])
            ->add('type', ChoiceType::class, array(
                'choices' => array(
                    'Particulier' => 'Particulier',
                    'Association' => 'Association',
                    'Entreprise' => 'Entreprise',
                ),
                'expanded' => true,
                'label_attr' => [
                    'class' => 'radio-inline'
                ],
                'multiple' => false
            ))
            ->add('email', EmailType::class, [
                'label' => 'Email *'
            ])
            ->add('telephone', TextType::class, [
                'label' => 'Télephone *'
            ])
            ->add('adresse_site', TextType::class, [
                'required' => false
            ])
            ->add('projet', TextType::class, [
                'label' => 'Projet *',
                'row_attr' => [
                    'class' => 'projet'
                ],
            ])
            ->add('piecejointe', FileType::class, [
                'label' => 'Envoie de pièce jointe (PDF file, image)',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // every time you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                            'image/jpeg',
                            'image/png'

                        ],
                        'mimeTypesMessage' => 'Merci de selectionner un format de document valide',
                    ])
                ],
                'label_attr' => [
                    'class' => 'label-piece-jointe'
                ],

            ])
            ->add('Date_limite', DateType::class, [
                'help' => 'Date à laquelle le projet doit impérativement être opérationnel',
                'widget' => 'single_text',

            ])

            ->add('message', TextareaType::class,  [
                'attr' => ['cols' => 20, 'rows' => 5],
                'label' => 'Message *'
            ])
            ->add('captcha', CaptchaType::class, array(
                'width' => 200,
                'height' => 50,
                'length' => 6,
                'required' => true
            ))

            ->add('Envoyer', SubmitType::class, [
                'attr' => array('href' => '{{ path(\'questionnaire\') }}'),
                'label' => 'Envoyer et continuer'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
